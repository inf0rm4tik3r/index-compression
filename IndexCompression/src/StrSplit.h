#pragma once

#include <string>
#include <optional>

#include "StrUtil.h"

template<typename S>
class StrSplit
{
public:
	StrSplit(const S& str, const std::string& delimiter) : str_(str), delimiter_(delimiter)
	{
	}

	std::optional<std::string_view> next()
	{
		const auto next_delim_pos = str_.find(delimiter_, start_);
		// We might not find another delimiter in the string. Either..
		if (next_delim_pos == std::string::npos)
		{
			const auto str_len = str_.length();
			// because we already traversed the whole string.
			if (start_ == str_len)
			{
				return {};
			}
			// or we didn't but there is still a last element to return.
			auto remainder = create_string_view(str_, start_, str_len);
			start_ = str_len;
			return {remainder};
		}
		// If we find the next delimiter position, return a view to the substring containing the next element.
		auto next_elem_view = create_string_view(str_, start_, next_delim_pos);
		start_ = next_delim_pos + delimiter_.length();
		return {next_elem_view};
	}

private:
	const S& str_;
	const std::string& delimiter_;
	size_t start_ = 0;
};

template<typename S>
StrSplit<S> str_split(const S& str, const std::string& delimiter)
{
	return StrSplit(str, delimiter);
}
