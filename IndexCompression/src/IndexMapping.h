#pragma once

#include <map>
#include <sstream>
#include <stdexcept>

class IndexMapping
{
public:
	IndexMapping() = default;

	explicit IndexMapping(std::map<int, int> map) : map_(std::move(map))
	{
	}

	bool follow(const int new_index, int& update)
	{
		if (map_.contains(new_index))
		{
			const auto it = map_.find(new_index);
			update = it->second;
			return true;
		}
		return false;
	}

	bool follow_if(const int new_index, const std::function<bool(int)>& pred, int& update)
	{
		if (map_.contains(new_index))
		{
			if (const auto it = map_.find(new_index);
				pred(it->second))
			{
				update = it->second;
				return true;
			}
		}
		return false;
	}

	[[nodiscard]]
	bool contains(const int key) const
	{
		return map_.contains(key);
	}

	[[nodiscard]]
	bool contains(const int key, const int value) const
	{
		if (map_.contains(key))
		{
			if (const auto it = map_.find(key);
				it->second == value)
			{
				return true;
			}
		}
		return false;
	}

	void insert(const int new_index, const int old_index)
	{
		if (const bool not_already_present = map_.insert(
				std::make_pair(new_index, old_index)
			).second;
			!not_already_present)
		{
			std::stringstream msg;
			msg << "Index " << new_index << " already maps to something.";
			throw std::invalid_argument(msg.str());
		}
	}

	void erase(const int key)
	{
		map_.erase(key);
	}

	void erase(const std::map<int, int>::iterator& it)
	{
		map_.erase(it);
	}

	bool erase_if_present(const int key, int& removed)
	{
		if (contains(key))
		{
			const auto it = find(key);
			removed = it->second;
			erase(it);
			return true;
		}
		return false;
	}

	[[nodiscard]]
	std::map<int, int>::const_iterator find(const int key) const
	{
		return map_.find(key);
	}

	[[nodiscard]]
	int get_or_default(const int key, const int& default_value) const
	{
		const auto it = find(key);
		return it == end() ? default_value : it->second;
	}

	[[nodiscard]]
	std::map<int, int>::const_iterator begin() const
	{
		return map_.begin();
	}

	[[nodiscard]]
	std::map<int, int>::const_iterator end() const
	{
		return map_.end();
	}

	void clear()
	{
		map_.clear();
	}

	friend std::ostream& operator<<(std::ostream& os, const IndexMapping& index_mapping)
	{
		return os << index_mapping.map_;
	}

private:
	std::map<int, int> map_;

	std::map<int, int>::iterator find(const int key)
	{
		return map_.find(key);
	}

	std::map<int, int>::iterator begin()
	{
		return map_.begin();
	}

	std::map<int, int>::iterator end()
	{
		return map_.end();
	}
};
