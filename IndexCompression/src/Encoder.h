#pragma once

#include <cassert>

#include "Vec3.h"

// Precondition: x contains the smallest element!
bool is_encodable(const Vec3i& vec)
{
	const auto diff_y = vec.y - vec.x;
	const auto diff_z = vec.z - vec.x;
	// Check precondition 1.
	assert(diff_y > 0);
	assert(diff_z > 0);
	// Check precondition 2.
	return diff_y <= 31 && diff_z <= 31;
	// TODO: Return why exactly the vec is not encodable: either not rotated properly or too big difference of what..
}

// Precondition: x contains the smallest element!
void encode(Vec3i& vec)
{
	const auto diff_y = vec.y - vec.x;
	const auto diff_z = vec.z - vec.x;
	// Check precondition 1.
	assert(diff_y > 0);
	assert(diff_z > 0);
	// Check precondition 2.
	assert(diff_y <= 31);
	assert(diff_z <= 31);
	vec.y = diff_y;
	vec.z = diff_z;
}