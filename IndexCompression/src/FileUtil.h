#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include "IndexMapping.h"

inline std::string read_file(const std::filesystem::path& path)
{
	std::cout << "Reading file \"" << path.string() << "\"" << std::endl;
	auto fstream = std::ifstream(path, std::ios::in | std::ios::ate);
	if (!fstream.is_open())
	{
		std::stringstream msg;
		msg << "Unable to open file: " << path.string();
		throw std::runtime_error(msg.str());
	}
	const auto size = static_cast<size_t>(fstream.tellg());
	std::string buffer(size, ' ');
	fstream.seekg(0);
	fstream.read(buffer.data(), size);
	fstream.close();
	return buffer;
}

inline void write_triangles(const std::vector<Vec3i>& triangles, const std::filesystem::path& path)
{
	std::cout << "Writing file \"" << path.string() << "\"" << std::endl;
	auto fstream = std::ofstream(path, std::ios::out | std::ios::trunc);
	if (!fstream.is_open())
	{
		std::stringstream msg;
		msg << "Unable to open file: " << path.string();
		throw std::runtime_error(msg.str());
	}
	for (const auto& triangle : triangles)
	{
		fstream << triangle.x << ", " << triangle.y << ", " << triangle.z << "\n";
	}
	fstream.close();
}

inline void write_mapping(const IndexMapping& mapping, const std::filesystem::path& path)
{
	std::cout << "Writing file \"" << path.string() << "\"" << std::endl;
	auto fstream = std::ofstream(path, std::ios::out | std::ios::trunc);
	if (!fstream.is_open())
	{
		std::stringstream msg;
		msg << "Unable to open file: " << path.string();
		throw std::runtime_error(msg.str());
	}
	for (const auto& [new_index, old_index] : mapping)
	{
		fstream << new_index << " " << old_index << "\n";
	}
	fstream.close();
}
