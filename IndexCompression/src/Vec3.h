#pragma once

#include <sstream>

template <typename T, T T_ZERO, T T_ONE>
class Vec3
{
public:
	T x;
	T y;
	T z;

	Vec3() : x(T_ZERO), y(T_ZERO), z(T_ZERO)
	{
	}

	Vec3<T, T_ZERO, T_ONE>(const T x, const T y, const T z)
		: x(x), y(y), z(z)
	{
	}

	void rotate_left()
	{
		const int temp = x;
		x = y;
		y = z;
		z = temp;
	}

	void rotate_right()
	{
		const int temp = z;
		z = y;
		y = x;
		x = temp;
	}

	void rotate_min_value_to_front()
	{
		switch (index_of_min_value())
		{
			case 0:
				return;
			case 1:
				rotate_left();
				return;
			case 2:
				rotate_right();
				return;
			default:
				throw std::bad_exception();
		}
	}

	[[nodiscard]]
	T min() const
	{
		return std::min(x, std::min(y, z));
	}

	[[nodiscard]]
	T mid() const
	{
		return std::max(x, std::min(y, z));
	}

	[[nodiscard]]
	T max() const
	{
		return std::max(x, std::max(y, z));
	}

	T operator[](const size_t index) const
	{
		switch (index)
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		default:
			std::stringstream msg;
			msg << "Array index " << index << " is out of bounds for type Vec3.";
			throw std::out_of_range(msg.str());
		}
	}

	T& operator[](const size_t index)
	{
		switch (index)
		{
		case 0: return x;
		case 1: return y;
		case 2: return z;
		default:
			std::stringstream msg;
			msg << "Array index " << index << " is out of bounds for type Vec3.";
			throw std::out_of_range(msg.str());
		}
	}

	[[nodiscard]]
	size_t index_of_min_value() const
	{
		size_t min_index = 0;
		if (y < operator[](min_index))
		{
			min_index = 1;
		}
		if (z < operator[](min_index))
		{
			min_index = 2;
		}
		return min_index;
	}
};

template <typename T, T T_ZERO, T T_ONE>
bool operator==(const Vec3<T, T_ZERO, T_ONE>& a, const Vec3<T, T_ZERO, T_ONE>& b)
{
	return a.x == b.x
		&& a.y == b.y
		&& a.z == b.z;
}

using Vec3i = Vec3<int, 0, 1>;

inline std::ostream& operator<<(std::ostream& os, const Vec3i& foo)
{
	return os << "Vec3(x: " << foo.x << ", y: " << foo.y << ", z: " << foo.z << ")";
}
