#pragma once

#include <cassert>
#include <iostream>
#include <map>
#include <vector>

#include "TriangleGraph.h"
#include "Vec3.h"

class TriangleGraphTotalError
{
public:
	std::map<int, int> errors_by_triangle;
	int total_error;

	TriangleGraphTotalError(const TriangleGraph& graph)
	{
		// Sum up individual edge errors.
		total_error = 0;
		errors_by_triangle = {};

		// THIS ROUTINE TRAVERSES ONCE OVER ALL UNDIRECTED EDGES. IF DIRECTED EDGES EXIST, IT MAY OR MAY NOT INCLUDE THEM!
		// ONLY USE, IF ALL EDGES ARE UNDIRECTED.
		int i = 0;
		for (const auto& node : graph.nodes_)
		{
			for (const auto& edge : node.edges)
			{
				if (i < edge.dst)
				{
					if (const int edge_error = edge.edge_payload.error; edge_error > 0)
					{
						total_error += edge_error;
						for (const auto& participating_triangle_idx : edge.edge_payload.triangles)
							errors_by_triangle[participating_triangle_idx] += edge_error;
					}
				}
			}
			i++;
		}
	}

	int triangle_with_lowest_error() const
	{
		assert(!errors_by_triangle.empty());
		int idx = -1;
		int min_error = 999999999; // TODO: use max value..
		for (auto const& [triangle_idx, triangle_error] : errors_by_triangle)
		{
			if (triangle_error != 0 && triangle_error < min_error)
			{
				min_error = triangle_error;
				idx = triangle_idx;
			}
		}
		return idx;
	}

	void print(const std::vector<Vec3i>& triangles) const
	{
		std::cout << "TriangleGraphTotalError {\n"
			<< "  total_error: " << total_error << ",\n"
			<< "  errors_by_triangle: {\n";
		for (const auto& [triangle_idx, triangle_error] : errors_by_triangle)
		{
			std::cout << "    " << triangle_idx << "(being: " << triangles[triangle_idx] << "  ->  " << triangle_error
				<< "\n";
		}
		std::cout << "  }\n}";
	}
};
