#pragma once

#include <utility>
#include <vector>
#include <functional>
#include <sstream>

#include "TypeName.h"

template <typename EDGE_PAYLOAD>
struct Edge
{
	// Index into nodes_!
	int dst;
	EDGE_PAYLOAD edge_payload;
};

template <typename NODE_PAYLOAD, typename EDGE_PAYLOAD>
struct Node
{
	NODE_PAYLOAD node_payload;
	std::vector<Edge<EDGE_PAYLOAD>> edges;

	[[nodiscard]]
	bool has_edge_reaching(const int node_idx) const
	{
		for (const auto& edge : edges)
		{
			if (edge.dst == node_idx)
			{
				return true;
			}
		}
		return false;
	}

	void modify_edge_reaching(const int node_idx, std::function<void(EDGE_PAYLOAD&)> mod)
	{
		for (auto& edge : edges)
		{
			if (edge.dst == node_idx)
			{
				mod(edge.edge_payload);
				return;
			}
		}
		std::stringstream msg;
		msg << "There is not edge reaching the node at index " << node_idx;
		throw std::invalid_argument(msg.str());
	}
};

template <typename NODE_PAYLOAD, typename EDGE_PAYLOAD>
class Graph
{
public:
	Graph(std::function<int(const NODE_PAYLOAD&)> node_identifier) : node_identifier_(std::move(node_identifier))
	{
	}

	size_t add_node(NODE_PAYLOAD node_payload)
	{
		Node<NODE_PAYLOAD, EDGE_PAYLOAD> node{node_payload, {}};
		nodes_.emplace_back(node);
		return nodes_.size() - 1;
	}

	void add_node_if_not_present(NODE_PAYLOAD node_payload)
	{
		auto idx = find_node_idx(node_identifier_(node_payload));
		if (idx == -1)
		{
			add_node(node_payload);
		}
	}

	// Returns index into nodes_.
	[[nodiscard]]
	int find_node_idx(int identifier) const
	{
		int i = 0;
		for (const auto& node : nodes_)
		{
			if (node_identifier_(node.node_payload) == identifier)
			{
				return i;
			}
			i++;
		}
		return -1;
	}

	// TODO: Optimize the performance of this function, as it is heavily used.
	// TODO: Either use the identifier directly as in index into an internal node-structure or..
	// TODO: Create a map that maps identifiers to their nodes position in the node-structure. But that introduces another invariant and makes manipulation of the nodes harder.
	NODE_PAYLOAD& find_node(int identifier)
	{
		int i = 0;
		for (auto& node : nodes_)
		{
			if (node_identifier_(node.node_payload) == identifier)
			{
				return node.node_payload;
			}
			i++;
		}
		std::stringstream msg;
		msg << "No node for the given identifier " << identifier << " could be found!";
		throw std::invalid_argument(msg.str());
	}

	void add_directed_edge(const int node_a_idx, const int node_b_idx, EDGE_PAYLOAD edge_payload)
	{
		Edge<EDGE_PAYLOAD> edge{node_b_idx, edge_payload};
		nodes_[node_a_idx].edges.emplace_back(edge);
	}

	void modify_undirected_edge(const int identifier_a, const int identifier_b, std::function<void(EDGE_PAYLOAD&)> mod)
	{
		int node_a_idx = find_node_idx(identifier_a);
		int node_b_idx = find_node_idx(identifier_b);
		assert(node_a_idx >= 0);
		assert(node_b_idx >= 0);
		assert(nodes_[node_a_idx].has_edge_reaching(node_b_idx));
		assert(nodes_[node_b_idx].has_edge_reaching(node_a_idx));
		nodes_[node_a_idx].modify_edge_reaching(node_b_idx, mod);
		nodes_[node_b_idx].modify_edge_reaching(node_a_idx, mod);
	}

	void add_undirected_edge_if_not_present(const int identifier_a, const int identifier_b, EDGE_PAYLOAD edge_payload)
	{
		int node_a_idx = find_node_idx(identifier_a);
		int node_b_idx = find_node_idx(identifier_b);
		if (!nodes_[node_a_idx].has_edge_reaching(node_b_idx))
		{
			add_directed_edge(node_a_idx, node_b_idx, edge_payload);
		}
		if (!nodes_[node_b_idx].has_edge_reaching(node_a_idx))
		{
			add_directed_edge(node_b_idx, node_a_idx, edge_payload);
		}
	}

	// TODO: Make private and provide save ways to access and mutate the graph.
	// private:
	std::function<int(const NODE_PAYLOAD&)> node_identifier_;
	std::vector<Node<NODE_PAYLOAD, EDGE_PAYLOAD>> nodes_;
};

template <typename EDGE_PAYLOAD>
std::ostream& operator<<(std::ostream& os, const Edge<EDGE_PAYLOAD>& edge)
{
	return os << "{ "
		<< "dst: " << edge.dst
		<< ", payload: { " << edge.edge_payload << " }"
		<< " }";
}

template <typename EDGE_PAYLOAD>
std::ostream& operator<<(std::ostream& os, const std::vector<Edge<EDGE_PAYLOAD>>& edges)
{
	for (const auto& edge : edges)
	{
		os << edge << ",\n      ";
	}
	return os;
}

template <typename NODE_PAYLOAD, typename EDGE_PAYLOAD>
std::ostream& operator<<(std::ostream& os, const Node<NODE_PAYLOAD, EDGE_PAYLOAD>& node)
{
	os << "node {\n"
		<< "    payload: {\n"
		<< "      " << node.node_payload << "\n"
		<< "    },\n"
		<< "    edges: [\n"
		<< "      " << node.edges << "\n"
		<< "    ]\n"
		<< "  }";
	return os;
}

template <typename NODE_PAYLOAD, typename EDGE_PAYLOAD>
std::ostream& operator<<(std::ostream& os, const Graph<NODE_PAYLOAD, EDGE_PAYLOAD>& graph)
{
	os << "Graph<" << type_name<NODE_PAYLOAD>() << ", " << type_name<EDGE_PAYLOAD>() << "> {\n";
	for (const auto& node : graph.nodes_)
	{
		os << "  " << node << std::endl;
	}
	os << "}";
	return os;
}
