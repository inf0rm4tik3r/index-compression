#pragma once

#include <ostream>

// Swap index A (stored in nodes[node_idx_a]) with index B (stored in nodes[node_idx_b]).
// Member possible_error_correction stores the difference to the current error.
struct Move
{
	int index_a;
	int node_idx_a;
	int index_b;
	int node_idx_b;
	int error;
	int possible_error_correction;

	[[nodiscard]]
	bool is_inverse_of(const Move& other) const
	{
		return index_a == other.index_b && index_b == other.index_a;
	}
};

std::ostream& operator<<(std::ostream& os, const Move& move)
{
	return os << move.index_a << " <-> " << move.index_b;
}
