#pragma once

#include <charconv>
#include <iostream>
#include <string>
#include <sstream>
#include <optional>
#include <system_error>

#include "Result.h"
#include "StrSplit.h"
#include "StrUtil.h"

struct ParseError
{
	std::string msg;
	std::optional<std::errc> error_code;
};

template <typename S, typename T>
Result<T, ParseError> parse_number(const S& str)
{
	T x;
	if (auto [ptr, error_code] = std::from_chars(str.data(), str.data() + str.size(), x);
		ptr == str.data())
	{
		std::stringstream msg;
		msg << "Unable to parse \"" << str << "\" as int.";
		return Result<T, ParseError>::err(ParseError{msg.str(), error_code});
	}
	return Result<T, ParseError>::ok(x);
}

template <typename S, typename T>
T parse_number_or_fail(const S& str)
{
	const auto parse_result = parse_number<S, T>(str);
	if (parse_result.is_err())
	{
		throw std::invalid_argument(parse_result.get_error().msg);
	}
	return parse_result.get_value();
}

template <typename T>
T parse_number_or_fail(const std::string_view& str)
{
	return parse_number_or_fail<std::string_view, T>(str);
}

template <typename S>
Result<bool, ParseError> parse_bool(const S& str)
{
	if (str == "true")
	{
		return Result<bool, ParseError>::ok(true);
	}
	if (str == "false")
	{
		return Result<bool, ParseError>::ok(false);
	}
	const Result<int, ParseError> parse_result = parse_number<S, int>(str);
	if (parse_result.is_err())
	{
		std::stringstream msg;
		msg << "Unable to parse \"" << str
			<< R"(" as a boolean, because it was neither "true" nor "false" nor parsable as an int.)";
		return Result<bool, ParseError>::err(ParseError { msg.str(), parse_result.get_error().error_code });
	}
	return Result<bool, ParseError>::ok(parse_result.get_value() != 0);
}

template <typename S>
bool parse_bool_or_fail(const S& str)
{
	const auto parse_result = parse_bool(str);
	if (parse_result.is_err())
	{
		throw std::invalid_argument(parse_result.get_error().msg);
	}
	return parse_result.get_value();
}

inline bool parse_bool_or_fail(const std::string_view& str)
{
	return parse_bool_or_fail<std::string_view>(str);
}

/// Fails if the split contains no more parts or the next part can not be parsed as an int.
template <typename T, typename S>
T next_number(StrSplit<S>& split)
{
	const auto part = split.next();
	auto view = part.value();
	trim(view);
	return parse_number_or_fail<T>(view);
}

template <typename S, typename T, typename V>
V parse_as_vec3(const S& str, const std::string& delimiter)
{
	auto split = str_split(str, delimiter);
	const auto x = next_number<T>(split);
	const auto y = next_number<T>(split);
	const auto z = next_number<T>(split);
	return V(x, y, z);
}

template <typename V, typename T>
std::vector<V> parse_lines_as_vec3(const std::string& contents)
{
	const std::string delimiter = ",";
	std::vector<V> vectors;
	std::istringstream f(contents); // TODO: Performance? Use StrSplit instead?
	std::string line;
	while (std::getline(f, line))
	{
		if (!is_blank(line))
		{
			auto value = parse_as_vec3<std::string, T, V>(line, delimiter);
			vectors.emplace_back(value);
		}
	}
	return vectors;
}

template <typename S, typename K, typename V>
std::pair<K, V> parse_as_pair(const S& str, const std::string& delimiter)
{
	auto split = str_split(str, delimiter);
	const auto first = next_number<K>(split);
	const auto second = next_number<V>(split);
	return std::make_pair(first, second);
}

template <typename K, typename V>
std::map<K, V> parse_lines_as_map(const std::string& contents, const std::string& delimiter)
{
	std::map<K, V> map;
	std::istringstream f(contents);
	std::string line;
	while (std::getline(f, line))
	{
		if (!is_blank(line))
		{
			const bool result = map.insert(parse_as_pair<std::string, K, V>(line, delimiter)).second;
			assert(result); // No duplicate keys!
		}
	}
	return map;
}
