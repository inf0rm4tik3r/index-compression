#pragma once

#include <cassert>
#include <ostream>
#include <vector>

#include "Graph.h"
#include "Vec3.h"
#include "LogUtil.h"

struct NodePayload
{
	/// The `index` stored in this node. It is an index into an array of vertex data.
	int index;

	/// This vector contains indices into the original triangle structure
	///	of all triangles having once corner being this nodes `index`.
	std::vector<int> triangles;
};

inline std::ostream& operator<<(std::ostream& os, const NodePayload& node)
{
	return os << "index: " << node.index << ", triangles: " << node.triangles;
}

struct EdgePayload
{
	EdgePayload(const int error) : error(error), triangles({})
	{
	}

	/// The amount by which the distance between the indices of the nodes connection this edge
	/// exceed the `max_distance` the last error-evaluation ran with.
	int error;

	/// This vector contains indices into the original triangle structure
	/// of all triangles being part of / connected by this edge.
	std::vector<int> triangles;
};

inline std::ostream& operator<<(std::ostream& os, const EdgePayload& edge)
{
	return os << "err: " << edge.error << ", triangles: " << edge.triangles;
}

using TriangleGraph = Graph<NodePayload, EdgePayload>;

/// Computes the ERROR between the two indices `index_a` and `index_b` given a `max_difference`.
/// The error is either 0 or some positive number.
int compute_index_error(const int index_a, const int index_b, const int max_difference)
{
	assert(index_a >= 0);
	assert(index_b >= 0);
	const auto err = std::max(0, std::abs(index_a - index_b) - max_difference);
	assert(err >= 0);
	return err;
}

TriangleGraph construct_graph(const std::vector<Vec3i>& triangles, const int max_difference)
{
	auto node_identifier = [](const NodePayload& payload) -> int
	{
		return payload.index;
	};
	TriangleGraph graph { node_identifier };

	int i = 0;
	for (const auto& triangle : triangles)
	{
		graph.add_node_if_not_present(NodePayload { triangle.x, std::vector<int>() });
		graph.add_node_if_not_present(NodePayload { triangle.y, std::vector<int>() });
		graph.add_node_if_not_present(NodePayload { triangle.z, std::vector<int>() });
		graph.find_node(triangle.x).triangles.emplace_back(i);
		graph.find_node(triangle.y).triangles.emplace_back(i);
		graph.find_node(triangle.z).triangles.emplace_back(i);
		i++;
	}

	i = 0;
	for (const auto& triangle : triangles)
	{
		graph.add_undirected_edge_if_not_present(
			triangle.x, triangle.y,
			EdgePayload(compute_index_error(triangle.x, triangle.y, max_difference))
		);
		graph.add_undirected_edge_if_not_present(
			triangle.x, triangle.z,
			EdgePayload(compute_index_error(triangle.x, triangle.z, max_difference))
		);
		graph.add_undirected_edge_if_not_present(
			triangle.y, triangle.z,
			EdgePayload(compute_index_error(triangle.y, triangle.z, max_difference))
		);
		const auto mod = [i](EdgePayload& payload) -> void
		{
			payload.triangles.emplace_back(i);
		};
		graph.modify_undirected_edge(triangle.x, triangle.y, mod);
		graph.modify_undirected_edge(triangle.x, triangle.z, mod);
		graph.modify_undirected_edge(triangle.y, triangle.z, mod);

		i++;
	}
	return graph;
}