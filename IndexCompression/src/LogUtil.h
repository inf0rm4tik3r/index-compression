#pragma once

#include <ostream>
#include <map>
#include <set>
#include <vector>

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::map<T, U>& container)
{
	//os << "map<" << type_name<T>() << ", " << type_name<U>() << "> (\n";
	os << "map (" << std::endl;
	for (const auto [key, value] : container)
	{
		os << "  " << key << " -> " << value << "," << std::endl;
	}
	os << ")";
	return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& container)
{
	const size_t size = container.size();
	if (size == 0)
	{
		return os << "[]";
	}
	os << "[";
	for (size_t i = 0; i < size - 1; i++)
	{
		os << container[i] << ", ";
	}
	os << container[size - 1];
	os << "]";
	return os;
}

template <typename T, size_t SIZE>
std::ostream& operator<<(std::ostream& os, const std::array<T, SIZE>& container)
{
	if (SIZE == 0)
	{
		return os << "[]";
	}
	os << "[";
	for (size_t i = 0; i < SIZE - 1; i++)
	{
		os << container[i] << ", ";
	}
	os << container[SIZE - 1];
	os << "]";
	return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& container)
{
	const size_t size = container.size();
	if (size == 0)
	{
		return os << "[]";
	}
	os << "[";
	size_t i = 0;
	for (const auto& elem : container)
	{
		os << elem;
		if (i < size - 1)
		{
			os << ", ";
		}
		i++;
	}
	os << "]";
	return os;
}
