#pragma once

#include <ostream>

enum class [[nodiscard]] MoveError
{
	NO_MOVES,
	ALL_MOVES_EXHAUSTED,
	NOT_BETTER,
	INVERSE,
	CYCLE,
	TOO_MANY_NON_CORRECTING_MOVES,
};

inline std::ostream& operator<<(std::ostream& os, const MoveError& move_error)
{
	os << "MoveError: ";
	switch (move_error)
	{
	case MoveError::NO_MOVES:
		return os << "No possible moves were found...";
	case MoveError::ALL_MOVES_EXHAUSTED:
		return os << "All possible moves were not suitable...";
	case MoveError::NOT_BETTER:
		return os << "No move was found that would not make the current solution worse...";
	case MoveError::INVERSE:
		return os << "The best move found would be the inverse of the last move, and we cannot run infinite...";
	case MoveError::CYCLE:
		return os << "The move would create a cycle of recurring moves...";
	case MoveError::TOO_MANY_NON_CORRECTING_MOVES:
		return os << "Already performed too many operations without a reducing the overall error. Moving on...";
	}
	return os << "Implementation error: Missing case in switch statement.";
}
