#pragma once

#include <string>
#include <vector>

#include "StrSplit.h"
#include "StrUtil.h"

// TODO: Does not yet match "-cf", if both "-c" and "-f" are supported, as individual c and f arguments! Arg is thrown away.

struct SupportedArg
{
	const std::string long_name;
	const std::optional<std::string> short_name;
	const bool needs_value;
};

class SupportedArgs
{
public:
	SupportedArgs(const std::initializer_list<SupportedArg> supported_args)
		: supported_args_(supported_args)
	{
	}

	[[nodiscard]]
	size_t size() const
	{
		return supported_args_.size();
	}

	[[nodiscard]]
	std::vector<SupportedArg>::const_iterator begin() const
	{
		return supported_args_.begin();
	}

	[[nodiscard]]
	std::vector<SupportedArg>::const_iterator end() const
	{
		return supported_args_.end();
	}

private:
	const std::vector<SupportedArg> supported_args_;
};

//template <typename T>
struct Argument
{
	const std::string name;
	const std::optional<std::string_view> value;

	friend std::ostream& operator<<(std::ostream& os, const Argument& argument)
	{
		os << "Argument {name: " << argument.name << ", value: ";
		if (argument.value.has_value())
		{
			os << "SOME(" << argument.value.value() << ")";
		}
		else
		{
			os << "NONE";
		}
		os << "}";
		return os;
	}
};

class ArgMatcher
{
public:
	explicit ArgMatcher(SupportedArgs supported_args)
		: supported_args_(std::move(supported_args))
	{
	}

	std::optional<SupportedArg> match(const char* str) const
	{
		auto view = create_string_view(str);
		trim(view);
		remove_leading(dash, view);
		remove_from_first_of(equals, view);
		for (const auto& supported_arg : supported_args_)
		{
			// std::cout << "Matching \"" << view << "\" against \"" << supported_arg.long_name << "\"." << std::endl;
			if (supported_arg.short_name.has_value() && supported_arg.short_name.value() == view
				|| supported_arg.long_name == view)
			{
				return {supported_arg};
			}
		}
		return std::nullopt;
	}

	std::optional<std::string_view> extract_value(const char* str) const
	{
		auto view = create_string_view(str);
		trim(view);
		const auto first_equals = view.find_first_of(equals);
		if (first_equals == std::string::npos)
		{
			return std::nullopt;
		}
		remove_to_including_first_of(equals, view);
		return {view};
	}

private:
	const SupportedArgs supported_args_;
	static constexpr auto dash = "-";
	static constexpr auto equals = "=";
};

class ArgParser
{
public:
	ArgParser(SupportedArgs supported_args, const int argc, const char** argv)
		: arg_matcher_(ArgMatcher(std::move(supported_args))), argc_(argc), argv_(argv)
	{
	}

	std::optional<Argument> next()
	{
		// We reached the end of the arguments array.
		if (next_ == argc_)
		{
			return std::nullopt;
		}
		const auto arg = argv_[next_++];
		const auto maybe_supported_arg = arg_matcher_.match(arg);
		if (!maybe_supported_arg.has_value())
		{
			std::cerr << "WARN: Unable to match program argument \"" << arg << "\" to any known argument. Skipping it."
				<< std::endl;
			return next();
		}
		const auto& [long_name, short_name, needs_value] = maybe_supported_arg.value();
		if (needs_value)
		{
			if (const auto contained_value = arg_matcher_.extract_value(arg);
				contained_value.has_value())
			{
				return {Argument{long_name, contained_value}};
			}
			return {Argument{long_name, create_string_view(argv_[next_++])}};
		}
		return {Argument{long_name, std::nullopt}};
	}


private:
	const ArgMatcher arg_matcher_;
	const int argc_;
	const char** argv_;
	int next_ = 1;
};
