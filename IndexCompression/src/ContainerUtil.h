#pragma once

/// If `container` contains `key`, return the value that `key` maps to. Otherwise return the given `default_value`.
template <template<class, class, class...> class C, typename K, typename V, typename... Args>
V get_or_default(const C<K, V, Args...>& container, K const& key, const V& default_value)
{
	auto it = container.find(key);
	return it == container.end() ? default_value : it->second;
}
