#pragma once
#include <algorithm>
#include <ostream>
#include <set>
#include <vector>

#include "Move.h"

struct MoveBenefit
{
	Move move;
	int error_correction;
	std::set<int> triangles_participating_in_old_error;
	std::set<int> triangles_participating_in_new_error;
};

std::ostream& operator<<(std::ostream& os, const MoveBenefit& move)
{
	return os << move.move << " => error_correction: " << move.error_correction
		<< ", part_old: " << move.triangles_participating_in_old_error
		<< ", part_new: " << move.triangles_participating_in_new_error;
}

inline void sort_move_benefits(std::vector<MoveBenefit>& move_benefits)
{
	const struct
	{
		bool operator()(const MoveBenefit& a, const MoveBenefit& b) const
		{
			// error_correction - ascending
			if (a.error_correction < b.error_correction)
				return true;
			if (a.error_correction > b.error_correction)
				return false;

			// triangles_participating_in_new_error - ascending
			const auto part_a = a.triangles_participating_in_new_error.size();
			const auto part_b = b.triangles_participating_in_new_error.size();
			if (part_a < part_b)
				return true;
			if (part_a > part_b)
				return false;

			// move difference - ascending
			const int index_dif_a = std::abs(a.move.index_b - a.move.index_a);
			const int index_dif_b = std::abs(b.move.index_b - b.move.index_a);
			if (index_dif_a < index_dif_b)
				return true;
			if (index_dif_a > index_dif_b)
				return false;

			// Return false when equal!
			return false;
		}
	} compare_func;
	std::sort(
		move_benefits.begin(),
		move_benefits.end(),
		compare_func
	);
}
