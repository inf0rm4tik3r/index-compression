#pragma once

enum class OperationType
{
	MOVE,
	SWAP,
	REPLACEMENT,
};

inline std::ostream& operator<<(std::ostream& os, const OperationType& operation_type)
{
	switch (operation_type)
	{
	case OperationType::MOVE:
		return os << "MOVE";
	case OperationType::SWAP:
		return os << "SWAP";
	case OperationType::REPLACEMENT:
		return os << "REPLACEMENT";
	}
	// Switch is not exhaustive!
	throw std::bad_exception();
}