#pragma once

#include <array>

template <typename T, size_t SIZE>
class RingBuffer
{
public:
	void push(T value)
	{
		last_ = {buffer_[start_]};
		last_index_ = start_;
		buffer_[start_] = value;
		start_ = index_plus(start_, 1);
		elements_pushed_ += 1;
		if (!full_ && elements_pushed_ >= SIZE)
		{
			full_ = true;
		}
	}

	void pop()
	{
		if (!last_.has_value())
		{
			throw std::bad_exception();
		}
		start_ = last_index_;
		buffer_[start_] = last_.value();
		last_ = std::nullopt;
		elements_pushed_ -= 1;
		if (full_ && elements_pushed_ < SIZE)
		{
			full_ = false;
		}
	}

	[[nodiscard]]
	static size_t index_plus(const size_t index, const std::ptrdiff_t s_offset)
	{
		const auto s_index = static_cast<std::ptrdiff_t>(index);
		const size_t res = (s_index + s_offset) % SIZE;
		return res;
	}

	[[nodiscard]]
	static size_t index_minus(const size_t index, const std::ptrdiff_t s_offset)
	{
		return index_plus(index, -s_offset);
	}

	T operator[](const size_t index) const
	{
		return buffer_[(start_ + index) % SIZE];
	}

	T& operator[](const size_t index)
	{
		return buffer_[(start_ + index) % SIZE];
	}

	[[nodiscard]]
	std::array<T, SIZE>& get_buffer() const
	{
		return buffer_;
	}

	[[nodiscard]]
	static size_t get_index_of_last_insertion()
	{
		return SIZE - 1;
	}

	[[nodiscard]]
	bool is_full() const
	{
		return full_;
	}

	[[nodiscard]]
	size_t num_empty_slots() const
	{
		if (elements_pushed_ >= buffer_.size())
		{
			return 0;
		}
		return buffer_.size() - elements_pushed_;
	}

	[[nodiscard]]
	size_t num_full_slots() const
	{
		return buffer_.size() - num_empty_slots();
	}

	std::ostream& print_raw(std::ostream& os) const
	{
		return os << buffer_;
	}

	std::ostream& print(std::ostream& os) const
	{
		if (SIZE == 0)
		{
			return os << "[]";
		}
		os << "[";
		for (size_t i = 0; i < SIZE - 1; i++)
		{
			os << operator[](i) << ", ";
		}
		os << operator[](SIZE - 1);
		os << "]";
		return os;
	}

	friend std::ostream& operator<<(std::ostream& os, const RingBuffer<T, SIZE>& ring_buffer)
	{
		return ring_buffer.print(os);
	}

private:
	std::array<T, SIZE> buffer_{};
	std::optional<T> last_ = std::nullopt;
	size_t last_index_ = 0;
	size_t start_ = 0;
	size_t elements_pushed_ = 0;
	bool full_ = false;
};
