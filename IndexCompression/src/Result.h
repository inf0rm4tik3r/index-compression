#pragma once

template <typename T, typename E>
class Result
{
public:
	static Result ok(T value)
	{
		return Result(value);
	}

	static Result err(E error)
	{
		return Result(error);
	}

	[[nodiscard]]
	bool is_ok() const
	{
		return value_.has_value();
	}

	[[nodiscard]]
	bool is_err() const
	{
		return error_.has_value();
	}

	[[nodiscard]]
	T get_value() const
	{
		return value_.value();
	}

	[[nodiscard]]
	E get_error() const
	{
		return error_.value();
	}

	/// Drops whatever was stored in `error` and stores the given `value`, making this instance an OK result.
	void make_ok(T value)
	{
		value_.emplace(value);
		error_.reset();
	}

	/// Drops whatever was stored in `value` and stores the given `error`, making this instance an ERR result.
	void make_err(E error)
	{
		value_.reset();
		error_.emplace(error);
	}

private:
	explicit Result(T value) : value_({ value }), error_(std::nullopt)
	{
	}

	explicit Result(E error) : value_(std::nullopt), error_({ error })
	{
	}

	std::optional<T> value_;
	std::optional<E> error_;
};
