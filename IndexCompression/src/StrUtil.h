#pragma once

template <typename S>
std::string_view create_string_view(const S& src)
{
	return std::string_view(src);
}

template <typename S>
std::string_view create_string_view(const S& src, const size_t start, const size_t end)
{
	auto view = create_string_view(src);
	view.remove_prefix(start);
	view.remove_suffix(src.length() - end);
	return view;
}

template <typename T>
bool is_blank(const T& str)
{
	constexpr auto whitespaces = " \t\f\v\n\r";
	const auto first = str.find_first_not_of(whitespaces);
	return first >= str.length();
}

template <typename S>
void remove_leading(const S& chars, std::string_view& from_string_view)
{
	const auto front = from_string_view.find_first_not_of(chars);
	from_string_view.remove_prefix(std::min(front, from_string_view.size()));
}

template <typename S>
void remove_trailing(const S& chars, std::string_view& from_string_view)
{
	const auto back = from_string_view.find_last_not_of(chars);
	from_string_view.remove_suffix(from_string_view.size() - back - 1);
}

inline void trim(std::string_view& view)
{
	constexpr auto whitespaces = " \t\f\v\n\r";
	remove_leading(whitespaces, view);
	remove_trailing(whitespaces, view);
}

template <typename S>
void remove_from_first_of(const S& chars, std::string_view& view)
{
	const auto index = view.find_first_of(chars);
	if (index != std::string::npos)
	{
		view.remove_suffix(view.size() - index);
	}
}

template <typename S>
void remove_to_including_first_of(const S& chars, std::string_view& view)
{
	const auto index = view.find_first_of(chars);
	if (index != std::string::npos)
	{
		view.remove_prefix(index + 1); // TODO: chars.len()?
	}
}