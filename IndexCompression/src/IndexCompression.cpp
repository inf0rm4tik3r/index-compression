#include <iostream>
#include <filesystem>
#include <vector>
#include <optional>
#include <cassert>
#include <set>
#include <algorithm>

#include "ArgParser.h"
#include "TriangleGraph.h"
#include "TriangleGraphTotalError.h"
#include "LogUtil.h"
#include "Result.h"
#include "ParseUtil.h"
#include "Move.h"
#include "MoveBenefit.h"
#include "MoveError.h"
#include "FileUtil.h"
#include "IndexMapping.h"

#include "OperationType.h"
#include "CycleDetector.h"

struct CompactOperation
{
	int index_a;
	int index_b;
	OperationType op_type;

	[[nodiscard]]
	bool is_inverse_of(const Move& move) const
	{
		return index_a == move.index_b && index_b == move.index_a;
	}

	friend bool operator==(const CompactOperation& a, const CompactOperation& b)
	{
		return (a.index_a == b.index_a && a.index_b == b.index_b)
			|| (a.index_a == b.index_b && a.index_b == b.index_a)
			&& a.op_type == b.op_type;
	}

	friend std::ostream& operator<<(std::ostream& os, const CompactOperation& compact_op)
	{
		return os << compact_op.op_type << "{ " << compact_op.index_a << " - " << compact_op.index_b << "}";
	}
};

using OpCycleDetector = CycleDetector<CompactOperation, 10>;

struct NormalizationResult
{
	size_t amount_of_different_indices;
	int current_max_index;
};

/// The list of triangles may contains indices which do not start at 0
/// or are spaced out, leaving gaps between certain numbers.
///	This function fills the given `mapping` table, so that at the end,
///	`apply_reverse_mapping` can be called to bring all indices in the range [0..max_index].
NormalizationResult create_normalization_mapping(
	const std::vector<Vec3i>& triangles,
	IndexMapping& mapping)
{
	if (triangles.empty())
		return NormalizationResult{0, -1};

	// Compute individual indices.
	std::set<int> indices;
	for (const auto& triangle : triangles)
	{
		indices.emplace(triangle.x);
		indices.emplace(triangle.y);
		indices.emplace(triangle.z);
	}

	const int max_index = *indices.rbegin();
	// Early out if the indices are already tightly packed.
	if (indices.size() == static_cast<size_t>(max_index) - 1)
		return NormalizationResult{indices.size(), max_index};

	// Create mapping.
	int running = 0;
	for (auto& index : indices)
	{
		if (running < index)
			mapping.insert(running, index);
		running++;
	}

	return NormalizationResult{indices.size(), max_index};
}

/// Modifies the given `triangles` by looking each value of each triangle up as a key in the given `mapping` table.
///	If the table contains a mapping from the current value to some other value, the current value will be replaced by that other value.
/// If for any value, no mapping was found in the table, that value stays the same.
void apply_mapping(std::vector<Vec3i>& triangles,
                   const IndexMapping& mapping)
{
	for (auto& triangle : triangles)
	{
		for (int i = 0; i < 3; i++)
			// Map to value from mapping or keep current value.
			triangle[i] = mapping.get_or_default(triangle[i], triangle[i]);
	}
}

/// Modifies the given `triangles` by replacing all values of them,
///	which are part of the mapping table in the sense that "some key maps to that value",
///	with that key.
void apply_reverse_mapping(std::vector<Vec3i>& triangles,
                           const IndexMapping& mapping)
{
	for (auto& [key, value] : mapping)
	{
		for (auto& triangle : triangles)
		{
			// Map to value from mapping or keep current value.
			for (int i = 0; i < 3; i++)
			{
				if (triangle[i] == value)
					triangle[i] = key;
			}
		}
	}
}

/// Given a list of `vectors`, rotate each `Vec3i` so that its minimum value is stored in its x field.
void rotate_min_values_to_front(std::vector<Vec3i>& vectors)
{
	for (auto& triangle : vectors)
		triangle.rotate_min_value_to_front();
}

/// Given a list of `vectors`, sort them in ascending order over x, y.
void sort(std::vector<Vec3i>& vectors)
{
	const struct
	{
		bool operator()(const Vec3i& a, const Vec3i& b) const
		{
			if (const int d = a.x - b.x; d == 0)
			{
				return a.y < b.y;
			}
			return a.x < b.x;
		}
	} compare_func;
	std::sort(vectors.begin(), vectors.end(), compare_func);
}

std::vector<Move> compute_possible_moves(const TriangleGraph& graph,
                                         const std::vector<Vec3i>& triangles,
                                         const TriangleGraphTotalError& total_error)
{
	std::vector<Move> moves;

	// triangle -> real indices -> node lookup -> nodes of triangle indices -> reachable other nodes..

	// For every node in every triangle that is a participant of an error:
	// (note: Two triangle can share vertices! This might double them. So collect them in a set first.)
	//   For every other reachable node:
	//     Create a move
	// any constraints to limit the result set?
	std::set<int> individual_indices;
	for (const auto& [triangle_idx, _] : total_error.errors_by_triangle)
	{
		const auto& triangle = triangles[triangle_idx];
		individual_indices.emplace(triangle.x);
		individual_indices.emplace(triangle.y);
		individual_indices.emplace(triangle.z);
	}

	// THIS ROUTINE TRAVERSES ONCE OVER ALL UNDIRECTED EDGES. IF DIRECTED EDGES EXIST, IT MAY OR MAY NOT INCLUDE THEM!
	// ONLY USE, IF ALL EDGES ARE UNDIRECTED.
	for (const auto index : individual_indices)
	{
		const auto node_idx = graph.find_node_idx(index);
		const auto& node = graph.nodes_[node_idx];

		for (const auto& edge : node.edges)
		{
			if (node_idx < edge.dst)
			{
				moves.emplace_back(Move{
					node.node_payload.index,
					node_idx,
					graph.nodes_[edge.dst].node_payload.index,
					edge.dst,
					0, 0
				});
			}
		}
	}
	return moves;
}

// TODO: Use less arguments!! Make code neater...
/// Tests the given `move` in the given `graph` without it being mutable!
/// Only process the edges connected to the node `origin_node_idx`.
void test_node_manipulation(
	const Move& move,
	const TriangleGraph& graph,
	const int origin_node_idx,
	const int origin_node_payload_index,
	const bool origin_is_a,
	const bool origin_is_b,
	const int max_difference,
	// Out
	bool& found_a_b_edge,
	int& total_old_error,
	int& total_new_error,
	std::set<int>& prev_part_triangles,
	std::set<int>& new_part_triangles
)
{
	for (const auto& [dst_node_idx, edge_payload] : graph.nodes_[origin_node_idx].edges)
	{
		// If we look at the a-b edge: Only process it once!
		if ((origin_node_idx == move.node_idx_a && dst_node_idx == move.node_idx_b)
			|| (origin_node_idx == move.node_idx_b && dst_node_idx == move.node_idx_a))
		{
			if (found_a_b_edge)
				continue;
			found_a_b_edge = true;
		}

		// We need to swap out the index from the target if the node we look at is one of our "move" nodes.
		// Why?: We did not modify the graph / did not swap out the index values. So this must be faked here.
		const int from = origin_node_payload_index;
		int to = graph.nodes_[dst_node_idx].node_payload.index;
		if (origin_is_a && to == move.index_b)
			to = move.index_a;
		else if (origin_is_b && to == move.index_a)
			to = move.index_b;

		const int old_error = edge_payload.error;
		const int new_error = compute_index_error(from, to, max_difference);
		total_old_error += old_error;
		total_new_error += new_error;

		if (old_error > 0)
			for (const auto& idx : edge_payload.triangles)
				prev_part_triangles.emplace(idx);
		if (new_error > 0)
			for (const auto& idx : edge_payload.triangles)
				new_part_triangles.emplace(idx);
	}
}

/// Computes the "error correction" for the given `move`, given that two indices may only be `max_difference` appart from each other. 
/// That is the amount by which the overall error of the graph would shrink if that move was performed.
///	Bundle the move and result inside `MoveBenefit`.
MoveBenefit compute_move_benefit(const TriangleGraph& graph,
                                 const Move& move,
                                 const int max_difference)
{
	// Calculate new errors for all edges at the two nodes which participated in the move.
	int total_old_error = 0;
	int total_new_error = 0;
	std::set<int> prev_part_triangles;
	std::set<int> new_part_triangles;
	bool found_a_b_edge = false;

	// Test everything reachable from node A.
	test_node_manipulation(move, graph, move.node_idx_a, move.index_b, true, false, max_difference,
	                       found_a_b_edge, total_old_error, total_new_error, prev_part_triangles, new_part_triangles);

	// Test everything reachable from node B.
	test_node_manipulation(move, graph, move.node_idx_b, move.index_a, false, true, max_difference,
	                       found_a_b_edge, total_old_error, total_new_error, prev_part_triangles, new_part_triangles);

	// Compare the new error from all these edges to the old error of these edges.
	// We want error_correction to be negative if the error has shrunk!
	return MoveBenefit{
		move,
		total_new_error - total_old_error,
		prev_part_triangles,
		new_part_triangles
	};
}

/// Computes the `MoveBenefit` fo all moves in `possible_moves` by calling `compute_move_benefit`.
std::vector<MoveBenefit> compute_move_benefits(const std::vector<Move>& possible_moves,
                                               const TriangleGraph& graph,
                                               const int max_difference)
{
	std::vector<MoveBenefit> move_benefits;
	move_benefits.reserve(possible_moves.size());
	for (const auto& move : possible_moves)
	{
		move_benefits.emplace_back(compute_move_benefit(graph, move, max_difference));
	}
	sort_move_benefits(move_benefits);
	return move_benefits;
}

/// Given a `triangle` and an index into that triangle called `idx_to_replace` (being only 0, 1, or 2),
///	computes a range of possible indices which could be swapped to that position in the triangle
///	so that the triangle would no longer be in an error state.
void compute_valid_swap_indices(const Vec3i& triangle,
                                const size_t idx_to_replace,
                                const int max_distance,
                                std::set<std::pair<int, int>>& out)
{
	const size_t other_1 = (idx_to_replace + 1) % 3;
	const size_t other_2 = (idx_to_replace + 2) % 3;
	const int min = std::min(triangle[other_1], triangle[other_2]);
	const int max = std::max(triangle[other_1], triangle[other_2]);
	const int range_lower = std::max(0, max - max_distance);
	const int range_upper = min + max_distance;

	std::vector<int> indices;
	if (range_lower > range_upper)
	{
		return;
	}
	for (int i = range_lower; i <= range_upper; i++)
	{
		if (i != triangle[other_1] && i != triangle[other_2] && i != triangle[idx_to_replace])
		{
			assert(i >= 0);
			out.emplace(std::make_pair(triangle[idx_to_replace], i));
		}
	}
}

// TODO: Possibility: Widen the requirement that the error is gone after such a swap. But that could decrease performance dramatically. Where would we stop?..
/// Picks one triangle that still has an error and computes all possible swaps in the graph
/// which could make the error in that triangle go away.
/// A swap is considered a move where the the indices being moved are not directly connected by an edge.
std::vector<Move> compute_possible_swaps(
	const TriangleGraph& graph,
	const TriangleGraphTotalError& total_error,
	const std::vector<Vec3i>& triangles,
	const int max_difference)
{
	const int triangle_idx_lowest_err = total_error.triangle_with_lowest_error();
	const auto& tri = triangles[triangle_idx_lowest_err]; // TODO: for all, or only the one with the highest error?
	std::set<std::pair<int, int>> replacements;
	compute_valid_swap_indices(tri, 0, max_difference, replacements);
	compute_valid_swap_indices(tri, 1, max_difference, replacements);
	compute_valid_swap_indices(tri, 2, max_difference, replacements);

	std::vector<Move> possible_swaps;
	possible_swaps.reserve(replacements.size());
	for (const auto& [from, to] : replacements)
	{
		if (const int to_node_idx = graph.find_node_idx(to); to_node_idx >= 0)
		{
			possible_swaps.emplace_back(Move{
				from,
				graph.find_node_idx(from),
				to,
				to_node_idx,
				0, 0
			});
		}
	}
	return possible_swaps;
}

struct ReplaceOp
{
	int index;
	int replacement;
	bool created;
};

std::ostream& operator<<(std::ostream& os, const ReplaceOp& replace_op)
{
	return os << replace_op.index << " => " << replace_op.replacement << ", created new index: " << (
		replace_op.created ? "true" : "false");
}

// TODO: Create multiple operations and test them first. Select best replacement operation and perform that one, similar to how moves work.
// TODO: Manual graph manipulation...
/// Picks a triangle with an error in it and replaces one of its indices, producing the error,
///	with a new index. This index can either be an index that was alrady generated and is suitable for the current position
///	or an entirely new index.
ReplaceOp replace_index(
	const TriangleGraphTotalError& total_error,
	std::vector<Vec3i>& triangles,
	IndexMapping& mapping,
	std::vector<int>& new_indices,
	int& max_index,
	std::optional<CompactOperation>& last_op,
	OpCycleDetector& cycle_detector)
{
	// TODO: Metric performance.. Should we try other triangles? 
	const int triangle_idx_lowest_err = total_error.triangle_with_lowest_error();
	auto& tri = triangles[triangle_idx_lowest_err];

	const auto min_idx = tri.index_of_min_value();

	const int original_old_index = tri[min_idx];
	int old_index = tri[min_idx];

	// If the index we want to map to, old_index, is already mapping onto something
	// that something must be the actual mapping target for our key to insert, replacement,
	// as only this will map the new index onto a "original"/"final" index.
	mapping.follow(old_index, old_index);

	int new_index = -1;
	bool created_new_index = false;

	// Check if there is a new index, which was already generated,
	// that currently maps onto the final index we want to replace.
	// If that is the case, re-use that new index. Otherwise create a new one.
	for (const auto& already_generated : new_indices)
	{
		if (already_generated != original_old_index)
		{
			if (mapping.contains(already_generated, old_index))
			{
				new_index = already_generated;
				break;
			}
		}
	}

	// If we could not find an existing mapping or if the mapping found would lead to a swap inverse to the las swap,
	// enforce the creation of a new index.
	if (new_index == -1
		|| (last_op.has_value()
			&& last_op.value().index_a == new_index
			&& last_op.value().index_b == original_old_index))
	{
		max_index += 1;
		created_new_index = true;
		new_indices.emplace_back(max_index);
		new_index = max_index;

		// Update the mapping table.
		mapping.insert(new_index, old_index);
	}

	// Update the triangle at hand.
	tri[min_idx] = new_index;

	const auto replace_op = ReplaceOp{ original_old_index, new_index, created_new_index };
	auto compact_op = CompactOperation{ replace_op.index, replace_op.replacement, OperationType::REPLACEMENT };
	last_op.emplace(compact_op);
	// Cycles should be allowed here, as we may have to lift the same node with the same replacement index multiple times!
	// (For different triangles)
	cycle_detector.just_push(compact_op);
	return replace_op;
}

/// Extracts, from a list of (validated) `possible_moves`, the best/optimal move to perform
///	or a `MoveError` if there is no optimal move, whatever the reason for that may be (see logging og MoveBenefit).
template <OperationType OP_TYPE>
Result<MoveBenefit, MoveError> extract_optimal_move(const std::vector<MoveBenefit>& possible_moves,
                                                    const std::optional<CompactOperation>& last_op,
													OpCycleDetector& cycle_detector)
{
	if (possible_moves.empty())
	{
		return Result<MoveBenefit, MoveError>::err(MoveError::NO_MOVES);
	}
	for (const auto& best_move : possible_moves)
	{
		if (best_move.error_correction > 0)
		{
			return Result<MoveBenefit, MoveError>::err(MoveError::NOT_BETTER);
		}
		if (last_op.has_value() && last_op.value().is_inverse_of(best_move.move))
		{
			//return Result<MoveBenefit, MoveError>::err(MoveError::INVERSE);
			continue;
		}
		if (cycle_detector.test(CompactOperation { best_move.move.index_a, best_move.move.index_b, OP_TYPE }).any_cycles)
		{
			//return Result<MoveBenefit, MoveError>::err(MoveError::CYCLE);
			continue;
		}
		return Result<MoveBenefit, MoveError>::ok(best_move);
	}
	return Result<MoveBenefit, MoveError>::err(MoveError::ALL_MOVES_EXHAUSTED);
}

// TODO: Perform move directly on a mutable graph. By doing that, we no longer need to reconstruct the graph before each operation.
/// Performs the given `move` operation.
/// In each triangle of `triangles`, each appearance of `move.index_a` is replaced with `move.index_b` and vice versa.
///	Also appropriately updates the mapping table to include the move.
void perform_move(const Move& move,
                  std::vector<Vec3i>& triangles,
                  IndexMapping& mapping)
{
	// Update triangle data.
	for (auto& triangle : triangles)
	{
		for (int i = 0; i < 3; i++)
		{
			if (triangle[i] == move.index_a)
				triangle[i] = move.index_b;
			else if (triangle[i] == move.index_b)
				triangle[i] = move.index_a;
		}
	}

	// Update the mapping information:
	// Note that both of the indices we are moving might already map to something else.
	// If that`s the case, for e.g., index_a already maps to some x, then index_b must not map to index_a but to that original value x.
	// Same goes for the other index, index_b.
	const int index_a = move.index_a;
	const int index_b = move.index_b;
	auto first_to_insert = std::pair(index_a, index_b);
	auto second_to_insert = std::pair(index_b, index_a);
	mapping.erase_if_present(index_b, first_to_insert.second);
	mapping.erase_if_present(index_a, second_to_insert.second);
	if (first_to_insert.first != first_to_insert.second)
	{
		mapping.insert(first_to_insert.first, first_to_insert.second);
	}
	if (second_to_insert.first != second_to_insert.second)
	{
		mapping.insert(second_to_insert.first, second_to_insert.second);
	}
}

/// Tries to perform the given `move`,
/// given the amount of previous move-operations which did not shrunk the error any further
/// and the las move that was performed.
///	These variables might still hinder us from executing the given move,
///	even if it was deemed the best move possible!
template <OperationType OP_TYPE>
std::optional<MoveError> try_perform_move(
	const Result<MoveBenefit, MoveError>& move,
	std::vector<Vec3i>& triangles,
	IndexMapping& mapping,
	size_t& last_moves_without_ec,
	const size_t& max_moves_without_ec,
	std::optional<CompactOperation>& last_op,
	OpCycleDetector& cycle_detector)
{
	if (move.is_err())
		return {move.get_error()};
	const auto& move_benefit = move.get_value();
	if (last_moves_without_ec < max_moves_without_ec ||
		(last_moves_without_ec == max_moves_without_ec && move_benefit.error_correction < 0))
	{
		if (move_benefit.error_correction >= 0)
			last_moves_without_ec++;
		else
			last_moves_without_ec = 0;
		auto op = CompactOperation{ move_benefit.move.index_a, move_benefit.move.index_b, OP_TYPE };
		cycle_detector.push_assert_no_cycles(op);
		perform_move(move_benefit.move, triangles, mapping);
		last_op = { op };
		return std::nullopt;
	}
	return {MoveError::TOO_MANY_NON_CORRECTING_MOVES};
}

/// Verifies that the constructed solution,
/// where all triangles are stored in `solution_triangles` and the mapping is stored in `solution_mapping_file`,
///	is equal to the initial set of triangles stored in `input_file`.
void verify_solution(const std::filesystem::path& input_file,
                     const std::filesystem::path& solution_triangles_file,
                     const std::filesystem::path& solution_mapping_file)
{
	auto original_triangles = parse_lines_as_vec3<Vec3i, int>(read_file(input_file));
	auto reorganized_triangles = parse_lines_as_vec3<Vec3i, int>(read_file(solution_triangles_file));
	const auto reorganized_mapping = IndexMapping(parse_lines_as_map<int, int>(read_file(solution_mapping_file), " "));

	// Bringing both structures in the same layout...
	rotate_min_values_to_front(original_triangles);
	sort(original_triangles);

	apply_mapping(reorganized_triangles, reorganized_mapping);
	rotate_min_values_to_front(reorganized_triangles);
	sort(reorganized_triangles);

	// Asserting equality!
	assert(original_triangles.size() == reorganized_triangles.size());
	for (size_t i = 0; i < original_triangles.size(); i++)
	{
		if (original_triangles[i] != reorganized_triangles[i])
		{
			std::cerr << "Verification failed at index " << i << std::endl;
			throw std::bad_exception(); // TODO: Throw some other exception.
		}
	}
}

struct Config
{
	int max_difference = 15;
	std::string input_file = "res\\indices.txt";
	std::string output_file = "out\\reorganized.txt";
	std::string output_mapping_file = "out\\reorganized_mapping.txt";
	bool perform_normalization = true;
	size_t max_moves_without_ec = 5;
	size_t max_swaps_without_ec = 5;
	size_t operation_limit = 20000;

	friend std::ostream& operator<<(std::ostream& os, const Config& config)
	{
		return os << "Config {" << std::endl
			<< "  max_difference: " << config.max_difference << "," << std::endl
			<< "  input_file: " << config.input_file << "," << std::endl
			<< "  output_file: " << config.output_file << "," << std::endl
			<< "  output_mapping_file: " << config.output_mapping_file << "," << std::endl
			<< "  perform_normalization: " << (config.perform_normalization ? "true" : "false") << "," << std::endl
			<< "  max_moves_without_ec: " << config.max_moves_without_ec << "," << std::endl
			<< "  max_swaps_without_ec: " << config.max_swaps_without_ec << "," << std::endl
			<< "  operation_limit: " << config.operation_limit << "," << std::endl
			<< "}" << std::endl;
	}
};

static constexpr const char* version = "v0.1";

void print_help()
{
	std::cout << "HELP: ... todo" << std::endl;
}

void print_version()
{
	std::cout << version << std::endl;
}

int main(const int argc, const char* argv[])
{
	Config config;

	ArgParser arg_parser{
		SupportedArgs{
			{
				SupportedArg{"help", "h", false},
				SupportedArg{"version", "v", false},
				SupportedArg{"max-difference", "d", true},
				SupportedArg{"input-file", "i", true},
				SupportedArg{"output-file", "o", true},
				SupportedArg{"output-mapping-file", "m", true},
				SupportedArg{"perform-normalization", std::nullopt, true},
				SupportedArg{"max-moves-without-ec", std::nullopt, true},
				SupportedArg{"max-swaps-without-ec", std::nullopt, true},
				SupportedArg{"operation-limit", std::nullopt, true},
			}
		},
		argc, argv
	};

	while (true)
	{
		std::optional<Argument> next_arg = arg_parser.next();
		if (!next_arg.has_value())
		{
			break;
		}
		const auto& [name, value] = next_arg.value();
		if (name == "help")
		{
			print_help();
			return 0;
		}
		if (name == "version")
		{
			print_version();
			return 0;
		}
		if (name == "max-difference")
		{
			config.max_difference = parse_number_or_fail<int>(value.value());
			if (config.max_difference < 0)
			{
				throw std::invalid_argument("Supplied \"max-difference\" must be positive!");
			}
		}
		if (name == "input-file")
		{
			config.input_file = value.value();
		}
		if (name == "output-file")
		{
			config.output_file = value.value();
		}
		if (name == "output-mapping-file")
		{
			config.output_mapping_file = value.value();
		}
		if (name == "perform-normalization")
		{
			config.perform_normalization = parse_bool_or_fail(value.value());
		}
		if (name == "max-moves-without-ec")
		{
			config.max_moves_without_ec = parse_number_or_fail<size_t>(value.value());
		}
		if (name == "max-swaps-without-ec")
		{
			config.max_swaps_without_ec = parse_number_or_fail<size_t>(value.value());
		}
		if (name == "operation-limit")
		{
			config.operation_limit = parse_number_or_fail<size_t>(value.value());
		}
	}

	std::cout << config << std::endl;

	auto triangles = parse_lines_as_vec3<Vec3i, int>(read_file(config.input_file));

	size_t ops = 0;
	size_t last_moves_without_ec = 0;
	size_t last_swaps_without_ec = 0;
	std::optional<CompactOperation> last_op;
	auto cycle_detector = OpCycleDetector();
	IndexMapping mapping;
	std::vector<int> new_indices;

	// Perform initial normalization if requested.
	int max_index = -1;
	const NormalizationResult norm_result = create_normalization_mapping(triangles, mapping);
	std::cout << "Maximum index is: " << norm_result.current_max_index << std::endl;
	if (config.perform_normalization)
	{
		std::cout << "Normalizing the input." << std::endl;
		apply_reverse_mapping(triangles, mapping);
		max_index = static_cast<int>(norm_result.amount_of_different_indices);
		std::cout << "Initial mapping is: " << mapping << std::endl << std::endl;
	}
	else
	{
		mapping.clear();
		max_index = norm_result.current_max_index;
	}

	bool solution_found = false;
	while (true)
	{
		// TODO: Do not construct the graph in every iteration!!! Required: perform_move() can properly modify the graph.
		auto graph = construct_graph(triangles, config.max_difference);
		const auto total_error = TriangleGraphTotalError(graph);
		std::cout << "OPERATION: " << ops << "\t" << "| current error: " << total_error.total_error << std::endl;

		if (total_error.total_error == 0)
		{
			solution_found = true;
			std::cout << "  Found a solution! :-)" << std::endl;
			std::cout << "  Generated " << new_indices.size() << " new indices, being: " << new_indices << std::endl;
			std::cout << "  Performed " << ops << " operations to find the solution." << std::endl;
			break;
		}

		// Try a move...
		const auto possible_moves = compute_possible_moves(graph, triangles, total_error);
		const auto move_benefits = compute_move_benefits(possible_moves, graph, config.max_difference);
		const auto optimal_move = extract_optimal_move<OperationType::MOVE>(move_benefits, last_op, cycle_detector);
		const auto move_error = try_perform_move<OperationType::MOVE>(optimal_move, triangles, mapping, last_moves_without_ec,
		                                         config.max_moves_without_ec, last_op, cycle_detector);

		if (!move_error.has_value())
		{
			std::cout << "  => MOVE: " << optimal_move.get_value() << std::endl;
		}
		else
		{
			// Try a swap...
			const auto possible_swaps = compute_possible_swaps(graph, total_error, triangles, config.max_difference);
			const auto swap_benefits = compute_move_benefits(possible_swaps, graph, config.max_difference);
			const auto optimal_swap = extract_optimal_move<OperationType::SWAP>(swap_benefits, last_op, cycle_detector);
			const auto swap_error = try_perform_move<OperationType::SWAP>(optimal_swap, triangles, mapping, last_swaps_without_ec,
			                                         config.max_swaps_without_ec, last_op, cycle_detector);
			if (!swap_error.has_value())
			{
				std::cout << "  => SWAP: " << optimal_swap.get_value() << std::endl;
			}
			else
			{
				// Try a replacement ...
				const ReplaceOp replace_op = replace_index(
					total_error, triangles, mapping, new_indices, max_index, last_op, cycle_detector
				);
				std::cout << "  => REPLACE: " << replace_op << std::endl;
			}
		}

		if (ops == config.operation_limit)
		{
			std::cerr << std::endl << "Could not find a solution. :-(" << std::endl;
			break;
		}
		ops++;
		//std::cout << std::endl;
	}

	if (solution_found)
	{
		// Rotation and sorting.
		std::cout << std::endl << "Rotating and sorting triangles." << std::endl;
		rotate_min_values_to_front(triangles);
		sort(triangles);

		// Write solution to disk.
		write_triangles(triangles, config.output_file);
		write_mapping(mapping, config.output_mapping_file);

		// Verification of the solution.
		std::cout << std::endl << "Verifying that the solution is correct..." << std::endl;
		verify_solution(config.input_file, config.output_file, config.output_mapping_file);
		std::cout << std::endl;
		std::cout << "Solution is correct! :-)" << std::endl;
	}

	std::cout << std::endl;
	std::cout << "Program finished." << std::endl;
}
