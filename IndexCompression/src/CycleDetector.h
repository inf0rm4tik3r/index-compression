#pragma once

#include <iostream>

#include "RingBuffer.h"

template <size_t SIZE>
struct DetectionResult
{
	const std::array<bool, SIZE> cycles;
	const bool any_cycles;

	[[nodiscard]]
	bool has_any_cycles() const
	{
		return any_cycles;
	}

	[[nodiscard]]
	bool has_no_cycles() const
	{
		return !any_cycles;
	}

	friend std::ostream& operator<<(std::ostream& os, const DetectionResult<SIZE>& detection_result)
	{
		return os << "DetectionResult { cycles: " << detection_result.cycles << " }";
	}
};

template <typename T, size_t SIZE>
class CycleDetector
{
public:

	[[nodiscard]]
	DetectionResult<SIZE> test(T value)
	{
		buffer_.push(value);
		auto result = compute_cycles();
		buffer_.pop();
		return result;
	}

	void just_push(T value)
	{
		buffer_.push(value);
	}

	[[nodiscard]]
	DetectionResult<SIZE> push(T value)
	{
		buffer_.push(value);
		return compute_cycles();
	}

	void push_assert_no_cycles(T value)
	{
		buffer_.push(value);
		assert(compute_cycles().has_no_cycles());
	}

	friend std::ostream& operator<<(std::ostream& os, const CycleDetector<T, SIZE>& cycle_detector)
	{
		os << "CycleDetector { " << std::endl
			<< "  buffer:     ";
		cycle_detector.buffer_.print(os);
		os << "," << std::endl
			<< "  buffer_raw: ";
		cycle_detector.buffer_.print_raw(os);
		os << std::endl
			<< "}";
		return os;
	}

private:
	[[nodiscard]]
	DetectionResult<SIZE> compute_cycles() const
	{
		const size_t full_slots = buffer_.num_full_slots();
		std::array<bool, SIZE> cycles{};
		bool any_cycles = false;
		for (size_t i = 0; i < SIZE; i++)
		{
			const size_t distance = i + 1;
			if (2 * distance <= full_slots)
			{
				bool result = has_cycle(distance);
				cycles[i] = result;
				if (result)
					any_cycles = true;
			}
			else
			{
				cycles[i] = false;
			}
		}
		return DetectionResult<SIZE>{cycles, any_cycles};
	}

	[[nodiscard]]
	bool has_cycle(const size_t distance) const
	{
		assert(distance >= 1); // to allow -1
		size_t last = buffer_.get_index_of_last_insertion();
		size_t cur_bucket_index = buffer_.index_minus(last, distance - 1);
		size_t prev_bucket_index = buffer_.index_minus(last, 2 * distance - 1);
		for (size_t i = 0; i < distance; i++)
		{
			if (buffer_[cur_bucket_index] != buffer_[prev_bucket_index])
			{
				return false;
			}
			cur_bucket_index = buffer_.index_plus(cur_bucket_index, 1);
			prev_bucket_index = buffer_.index_plus(prev_bucket_index, 1);
		}
		return true;
	}

	RingBuffer<T, 2 * SIZE> buffer_;
};
